curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/ca441a35-0672-4b51-83b7-109fb3661ec6/acknowledge'

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/f950eb54-1acf-47a5-9c93-91d0bd4ede24/acknowledge'


# {"updateId":"ca441a35-0672-4b51-83b7-109fb3661ec6","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":true,"statusCode":{"statusCode":"SENDING"},"updateTime":"2017-12-19T18:49:31.330Z","hasError":false}
# {"updateId":"f950eb54-1acf-47a5-9c93-91d0bd4ede24","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":true,"statusCode":{"statusCode":"SENT_TO_PLATFORM"},"updateTime":"2017-12-19T18:49:31.934Z","hasError":false}

# more updates from GET statusupdates
# [{"updateId":"05efbea4-8e27-4bc3-8da3-15b49cba2cf1","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":false,"statusCode":{"statusCode":"SENT_TO_TRANSLATOR"},"updateTime":"2017-12-19T18:50:34.426Z","hasError":false},{"updateId":"f52341ed-853b-400d-a5d8-7c0909103441","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":false,"statusCode":{"statusCode":"IN_TRANSLATION"},"updateTime":"2017-12-19T18:51:35.902Z","hasError":false},{"updateId":"3193bed0-54bd-4a33-914e-785c4547b473","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":false,"statusCode":{"statusCode":"REVIEW_TRANSLATION"},"updateTime":"2017-12-19T18:53:32.776Z","hasError":false}]

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/05efbea4-8e27-4bc3-8da3-15b49cba2cf1/acknowledge'

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/f52341ed-853b-400d-a5d8-7c0909103441/acknowledge'

curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates/3193bed0-54bd-4a33-914e-785c4547b473/acknowledge'

# {"updateId":"05efbea4-8e27-4bc3-8da3-15b49cba2cf1","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":true,"statusCode":{"statusCode":"SENT_TO_TRANSLATOR"},"updateTime":"2017-12-19T18:50:34.426Z","hasError":false}
# {"updateId":"f52341ed-853b-400d-a5d8-7c0909103441","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":true,"statusCode":{"statusCode":"IN_TRANSLATION"},"updateTime":"2017-12-19T18:51:35.902Z","hasError":false}
# {"updateId":"3193bed0-54bd-4a33-914e-785c4547b473","jobId":"e093fbc0-ddbc-4eef-9469-aad71f23ab38","requestIds":["e8e7dc48-678a-4eec-80a4-5955f222a82e"],"acknowledged":true,"statusCode":{"statusCode":"REVIEW_TRANSLATION"},"updateTime":"2017-12-19T18:53:32.776Z","hasError":false}