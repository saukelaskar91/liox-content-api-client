curl -X GET --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' 'https://content-api.lionbridge.com/v1/statusupdates'

# [
  # {
    # "updateId": "ca441a35-0672-4b51-83b7-109fb3661ec6",
    # "jobId": "e093fbc0-ddbc-4eef-9469-aad71f23ab38",
    # "requestIds": [
      # "e8e7dc48-678a-4eec-80a4-5955f222a82e"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "SENDING"
    # },
    # "updateTime": "2017-12-19T18:49:31.330Z",
    # "hasError": false
  # },
  # {
    # "updateId": "f950eb54-1acf-47a5-9c93-91d0bd4ede24",
    # "jobId": "e093fbc0-ddbc-4eef-9469-aad71f23ab38",
    # "requestIds": [
      # "e8e7dc48-678a-4eec-80a4-5955f222a82e"
    # ],
    # "acknowledged": false,
    # "statusCode": {
      # "statusCode": "SENT_TO_PLATFORM"
    # },
    # "updateTime": "2017-12-19T18:49:31.934Z",
    # "hasError": false
  # }
# ]
