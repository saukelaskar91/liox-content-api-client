# create a new job
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'Authorization: %%%USER_TOKEN_STRING%%%' -d '{
  "jobName": "kche new job 1220201715",
  "description": "ken che new rest api job",
  "poReference": "123456",
  "providerId": "%%%PROVIDER_ID_STRING%%%",
}' 'https://content-api.lionbridge.com/v1/jobs'

# {
  # "jobId": "3a197821-7675-4444-89d0-e67bc274df69",
  # "jobName": "kche new job 1220201715",
  # "description": "ken che new rest api job",
  # "statusCode": {
    # "statusCode": "CREATED"
  # },
  # "hasError": false,
  # "creatorId": "675492743",
  # "poReference": "123456",
  # "createdDate": "2017-12-20T20:35:52.133Z",
  # "modifiedDate": "2017-12-20T20:35:52.133Z",
  # "archived": false,
  # "providerId": "%%%PROVIDER_ID_STRING%%%"
# }

